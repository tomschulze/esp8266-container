## ESP8266 OCI container
* an OCI container enviroment for using the espressif SDK with esp8266 chipsets
* adapted from https://github.com/espressif/ESP8266_RTOS_SDK/tree/master/tools/docker
* build the image with e.g., `buildah bud -t esp8266`

## Use
* change to the directory containing the source files, then run:
  * `podman run -it --rm --device=/dev/ttyUSB0 --security-opt label=disable -v $PWD:/project -w /project esp8266 idf.py menuconfig` to configure the SDK parameters
  * `podman run --rm --device=/dev/ttyUSB0 --security-opt label=disable -v $PWD:/project -w /project esp8266 idf.py build` to build
  * `podman run --rm --device=/dev/ttyUSB0 --security-opt label=disable -v $PWD:/project -w /project esp8266 idf.py flash` to flash
  * `podman run -it --rm --device=/dev/ttyUSB0 --security-opt label=disable -v $PWD:/project -w /project esp8266 idf.py monitor` to monitor serial output
* for further help see https://github.com/espressif/ESP8266_RTOS_SDK

## Troubleshooting
* in order to pass the serial device through to the container I had to
  * disable SELinux with the `label=disable` flag in the `podman run` command and
  * add a new udev rule on the host machine which makes every `/dev/ttyUSB*` device read and write accessible for all
    * add `KERNEL=="ttyUSB[0-9]*",MODE="0666"` in `/etc/udev/rules.d/99-serial.rules`
    * https://www.losant.com/blog/how-to-access-serial-devices-in-docker
